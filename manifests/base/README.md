# manifests base

the gitlab registry secret contains the dockerconfig json which has the
following shape:

```json
{
  "auths": {
    "registry.gitlab.com": {
      "username": "user",
      "password": "password",
      "email": "user@example.com"
    }
  }
}
```
