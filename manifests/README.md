# manifests

kustomize manifests for deploying wirow to a k8s cluster


## extra steps in deploy

we need to mount the TLS certificate twice:

```shell
tls.crt --> /ssl/tls.crt
tls.key --> /ssl/tls.key

tls.crt --> /etc/letsencrypt/live/meet.wirow-test.io/fullchain.pem
tls.key --> /etc/letsencrypt/live/meet.wirow-test.io/privkey.pem
```

we accomplish this by: 
1. [mounting the TLS secret to the /ssl directory](base/wirow-deployment.yaml)
2. create a copy of this secret, renaming the tls.crt and tls/key appropriately,
   and mount this new secret to /etc/letsencrypt/live/meet.wirow-test.io/

```shell
rm -f tmp/wirow-tls-secret.yaml 

# retrieve the cert and private key from the cert-manager secret
kubectl get \
  secret wirow-domain-certificate-secret \
  -n wirow \
  -ogo-template='{{index .data "tls.crt"}}' \
  | base64 -d > tmp/fullchain.pem

kubectl get \
  secret wirow-domain-certificate-secret \
  -n wirow \
  -ogo-template='{{index .data "tls.key"}}' \
  | base64 -d > tmp/privkey.pem

# create duplicate wirow-ec secret
kubectl create \
  secret generic wirow-ec \
  -n wirow \
  --from-file=fullchain.pem=tmp/fullchain.pem \
  --from-file=privkey.pem=tmp/privkey.pem
  
kubectl create \
  secret generic wirow-ec \
  -n wirow \
  --from-file=fullchain.pem=tmp/fullchain.pem \
  --from-file=privkey.pem=tmp/privkey.pem
```
