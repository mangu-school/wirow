MAKEFLAGS += --warn-undefined-variables
SHELL := bash
.SHELLFLAGS := -o errexit -o nounset -o pipefail -c
.DEFAULT_GOAL := run
.DELETE_ON_ERROR:
.SUFFIXES:

include docker/Makefile
include manifests/Makefile

ci-validate:
	python3 -c 'import yaml, sys; print(yaml.safe_load(sys.stdin))' < .gitlab-ci.yml
	python3 -c 'import yaml, sys; print(yaml.safe_load(sys.stdin))' < .gitlab-ci-docker.yml
