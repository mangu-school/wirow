# generate ssl certs

We generate these for local testing.
In k8s these will be injected from a cert-manager certificate.
See the [k8s deployment](../../manifests/base/deployment.yaml).

```shell
openssl req \
  -x509 \
  -newkey rsa:4096 \
  -nodes \
  -out config/wirow-ecdsacert.pem \
  -keyout config/wirow-eckey.pem \
  -days 365
```

Input params for the dummy cert:
```shell
Country Name (2 letter code) []:US
State or Province Name (full name) []:DC
Locality Name (eg, city) []:DC
Organization Name (eg, company) []:wirow-test
Organizational Unit Name (eg, section) []:meet
Common Name (eg, fully qualified host name) []:meet.wirow-test.io
Email Address []: dev@wirow-test.io
```
