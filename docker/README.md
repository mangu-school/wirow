# wirow docker image

See the official [wirow repo][wirow] for patterns.

The [wirow binary](./wirow) included herein was obtained from the 
[wirow EAP program][try] and registered to the 
[creator of this commit message][@m.marwa].


[try]: https://wirow.io/#try
[wirow]: https://github.com/wirow-io/wirow-server/blob/master/docker/Dockerfile
